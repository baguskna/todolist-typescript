import React, { FunctionComponent } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Dashboard from './components/pages/Dashboard';
import Login from './components/pages/Login';

type Props = {
  children: any
}

const Guest: FunctionComponent<Props> = props => props.children;

const User: FunctionComponent<Props> = props => (localStorage.getItem('token') ? props.children : <Redirect to="/login" />);

const PrivateRoute: React.ComponentType<any> = ({
  auth: Auth,
  component: Component,
  ...rest
}) => (
    <Auth>
      <Route
        {...rest}
        render={props => (
          <Component {...props} />
        )}
      />
    </Auth>
  );

const App = () => {
  return (
    <Router>
      <Switch>
        <PrivateRoute
          path="/"
          component={Dashboard}
          auth={User}
          exact
        />
        <PrivateRoute
          path="/login"
          component={Login}
          auth={Guest}
        />
      </Switch>
    </Router>
  );
}

export default App;
