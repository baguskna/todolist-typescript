import React, { useState, FunctionComponent } from 'react';
import Header from '../components/Header';
import '../styles/Login.scss';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';

const Login: FunctionComponent = () => {
  const formDefaultValue = {
    username: "",
    password: ""
  };
  const [formValue, setFormValue] = useState(formDefaultValue);
  const { username, password } = formValue;

  const history = useHistory();

  const login = async () => {
    const result = await fetch('https://app-todoapps.herokuapp.com/users/login', {
      method: 'POST',
      body: JSON.stringify({
        username,
        password
      }),
      headers: { 'Content-Type': 'application/json' }
    });
    const resultJson = await result.json();
    if (resultJson.message === 'Login success') {
      localStorage.setItem("token", resultJson.result);
      history.push('/');
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: resultJson.message,
        backdrop: true
      })
    }
  }

  return (
    <div className="login">
      <Header title='Login' hasBackButton={false} />
      <div className="login-section">
        <div className="login-section--title">
          <h4>Insert Your username and password</h4>
        </div>
        <div className="form-submission">
          <p>Username :</p>
          <div className="form-submission--input">
            <input
              type="text"
              className="form-submission--input--form"
              name={"username"}
              placeholder="username..."
              value={username}
              onChange={(e) => {
                const target = e.target
                setFormValue(prevState => ({
                  ...prevState,
                  [target.name]: target.value
                }))
              }}
            />
          </div>
        </div>
        <div className="form-submission">
          <p>Password :</p>
          <div className="form-submission--input">
            <input
              type="password"
              className="form-submission--input--form"
              name={"password"}
              placeholder="password..."
              value={password}
              onChange={(e) => {
                const target = e.target
                setFormValue(prevState => ({
                  ...prevState,
                  [target.name]: target.value
                }))
              }}
            />
          </div>
        </div>
        <div className="button-wrap">
          <button
            className="button-wrap--post"
            type="button"
            onClick={login}
          >
            LOGIN
          </button>
        </div>
      </div>
    </div>
  )
}

export default Login;
