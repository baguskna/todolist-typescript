import React, { useEffect, useState, FunctionComponent } from 'react';
import Swal from 'sweetalert2';

const Dashboard: FunctionComponent = () => {
  const [items, setItems] = useState('');

  useEffect(() => {
    getData();
  }, []);

  const getData: any = async () => {
    const token: string | null = await localStorage.getItem("token");
    const data: any = await fetch('https://app-todoapps.herokuapp.com/todos', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    });
    const dataJson = await data.json();
    if (dataJson.message === 'Done') {
      setItems(dataJson.result);
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: dataJson.message,
        backdrop: true
      })
    }
  }

  return (
    <div>
      Dashboard
    </div>
  )
}

export default Dashboard;
