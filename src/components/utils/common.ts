export const createFetch = (path: string, options = {}) => (
  fetch(`https://app-todoapps.herokuapp.com/${path}`, {
    // method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
    ...options,
  }).then(async (res) => {
    const json = await res.json();
    console.log(json)
    // if (res.ok) return json;
    // return {
    //   message: json.message || 'AN_ERROR_OCCURED',
    //   status: res.status,
    //   ...json
    // }
  })
)

export default createFetch;
