import React, { FunctionComponent } from 'react';
import './Header.scss';

type HeaderProps = {
  title: string,
  hasBackButton: boolean
}

const Header: FunctionComponent<HeaderProps> = props => {
  return (
    <div className="header">
      <nav className="header--content">
        {
          props.hasBackButton ? (
            <div className="header--nav">
              <i className="fas fa-arrow-left"></i>
            </div>
          ) : null
        }
        <span className="header--title">
          {props.title}
        </span>
      </nav>
    </div>
  );
}

export default Header;
